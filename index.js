const entirePage = document.querySelector(".main-container");
let total = 9;

entirePage.addEventListener("click", function (event) {
  pageToggle(event.target.classList.value);
  monthlyYearlyToggle(event.target.classList.value);
  finishingUp(event.target, event.target.classList.value);
  finishingUpAddons(event.target, event.target.classList.value);

  if (total >= 90) {
    document.querySelector(".blue").innerText = `$${total}/yr`;
  } else {
    document.querySelector(".blue").innerText = `$${total}/mo`;
  }
});

function pageToggle(className) {
  switch (className) {
    case "next-step":
      document.querySelector(".step-1").classList.add("hide");
      document.querySelector(".step-2 ").classList.remove("hide");
      document
        .querySelector(".number_1")
        .classList.remove("sidebar_background_numbers");
      document
        .querySelector(".number_2")
        .classList.add("sidebar_background_numbers");
      break;
    case "go_back":
      document.querySelector(".step-2 ").classList.add("hide");
      document.querySelector(".step-1").classList.remove("hide");
      document
        .querySelector(".number_1")
        .classList.add("sidebar_background_numbers");
      document
        .querySelector(".number_2")
        .classList.remove("sidebar_background_numbers");
      break;
    case "next_step_2":
      document.querySelector(".step-2 ").classList.add("hide");
      document.querySelector(".step-3").classList.remove("hide");
      document
        .querySelector(".number_2")
        .classList.remove("sidebar_background_numbers");
      document
        .querySelector(".number_3")
        .classList.add("sidebar_background_numbers");
      break;
    case "go_back_3":
      document.querySelector(".step-3").classList.add("hide");
      document.querySelector(".step-2").classList.remove("hide");
      document
        .querySelector(".number_2")
        .classList.add("sidebar_background_numbers");
      document
        .querySelector(".number_3")
        .classList.remove("sidebar_background_numbers");
      break;
    case "next_step_3":
      document.querySelector(".step-3").classList.add("hide");
      document.querySelector(".step-4").classList.remove("hide");
      document
        .querySelector(".number_3")
        .classList.remove("sidebar_background_numbers");
      document
        .querySelector(".number_4")
        .classList.add("sidebar_background_numbers");
      break;
    case "go_back_4":
      document.querySelector(".step-4").classList.add("hide");
      document.querySelector(".step-3").classList.remove("hide");
      document
        .querySelector(".number_3")
        .classList.add("sidebar_background_numbers");
      document
        .querySelector(".number_4")
        .classList.remove("sidebar_background_numbers");
      break;
    case "confirm":
      document.querySelector(".step-4").classList.add("hide");
      document.querySelector(".step-5").classList.remove("hide");
  }
}

function monthlyYearlyToggle(className) {
  if (className === "slider round") {
    if (document.querySelector(".yearly-plans").classList.contains("hide")) {
      document.querySelector(".monthly-plans").classList.add("hide");
      document.querySelector(".yearly-plans").classList.remove("hide");

      document.querySelector(
        ".yearly-plans"
      ).parentElement.nextElementSibling.children[1].children[1].children[1].innerText =
        "+$10/yr";
      document.querySelector(
        ".yearly-plans"
      ).parentElement.nextElementSibling.children[2].children[1].children[1].innerText =
        "+$20/yr";
      document.querySelector(
        ".yearly-plans"
      ).parentElement.nextElementSibling.children[3].children[1].children[1].innerText =
        "+$20/yr";
    } else if (
      document.querySelector(".monthly-plans").classList.contains("hide")
    ) {
      document.querySelector(".yearly-plans").classList.add("hide");
      document.querySelector(".monthly-plans").classList.remove("hide");

      document.querySelector(
        ".monthly-plans"
      ).parentElement.nextElementSibling.children[1].children[1].children[1].innerText =
        "+$1/mo";
      document.querySelector(
        ".monthly-plans"
      ).parentElement.nextElementSibling.children[2].children[1].children[1].innerText =
        "+$2/mo";
      document.querySelector(
        ".monthly-plans"
      ).parentElement.nextElementSibling.children[3].children[1].children[1].innerText =
        "+$2/mo";
    }
  }
}

function finishingUp(target, className) {
  if (className === "plans") {
    let plan_name = target.children[1].children[0].innerText;
    let clas = target.children[1].children[0].className;
    let class_name = clas.split("-")[1];

    if (class_name === "monthly") {
      switch (plan_name) {
        case "Arcade":
          document.querySelector(
            ".plan_name_monthlyOrYearly"
          ).children[0].innerText = "Arcade(monthly)";

          document.querySelector(
            ".price_monthlyOrYearly"
          ).children[0].innerText = "$9/mo";
          total = 0;
          total += 9;
          break;
        case "Advanced":
          document.querySelector(
            ".plan_name_monthlyOrYearly"
          ).children[0].innerText = "Advanced(monthly)";

          document.querySelector(
            ".price_monthlyOrYearly"
          ).children[0].innerText = "$12/mo";
          total = 0;
          total += 12;
          break;
        case "Pro":
          document.querySelector(
            ".plan_name_monthlyOrYearly"
          ).children[0].innerText = "Pro(monthly)";

          document.querySelector(
            ".price_monthlyOrYearly"
          ).children[0].innerText = "$15/mo";
          total = 0;
          total += 15;
          break;
      }
    }

    if (class_name === "yearly") {
      switch (plan_name) {
        case "Arcade":
          document.querySelector(
            ".plan_name_monthlyOrYearly"
          ).children[0].innerText = "Arcade(yearly)";

          document.querySelector(
            ".price_monthlyOrYearly"
          ).children[0].innerText = "$90/yr";
          total = 0;
          total += 90;
          break;
        case "Advanced":
          document.querySelector(
            ".plan_name_monthlyOrYearly"
          ).children[0].innerText = "Advanced(yearly)";

          document.querySelector(
            ".price_monthlyOrYearly"
          ).children[0].innerText = "$120/yr";
          total = 0;
          total += 120;
          break;
        case "Pro":
          document.querySelector(
            ".plan_name_monthlyOrYearly"
          ).children[0].innerText = "Pro(Yearly)";

          document.querySelector(
            ".price_monthlyOrYearly"
          ).children[0].innerText = "$150/yr";
          total = 0;
          total += 150;
          break;
      }
    }
  }
}

function finishingUpAddons(target, className) {
  if (className === "container") {
    let add_on = target.children[0].children[0].innerText;
    let priceOfAddOn = target.children[1].innerText;

    switch (add_on) {
      case "Online service":
        if (
          document.querySelector(".addons_monthlyOrYearly").children[0]
            .children[0].innerText === "Online service"
        ) {
          document.querySelector(
            ".addons_monthlyOrYearly"
          ).children[0].children[0].innerText = "";
          document.querySelector(
            ".addons_monthlyOrYearly"
          ).children[0].children[1].innerText = "";
          let addOn_cost = parseInt(priceOfAddOn.split("/")[0].split("$")[1]);
          total -= addOn_cost;
        } else {
          document.querySelector(
            ".addons_monthlyOrYearly"
          ).children[0].children[0].innerText = add_on;
          document.querySelector(
            ".addons_monthlyOrYearly"
          ).children[0].children[1].innerText = priceOfAddOn;
          let addOn_cost = parseInt(priceOfAddOn.split("/")[0].split("$")[1]);
          total += addOn_cost;
        }
        break;
      case "Larger Storage":
        if (
          document.querySelector(".addons_monthlyOrYearly").children[1]
            .children[0].innerText === "Larger Storage"
        ) {
          document.querySelector(
            ".addons_monthlyOrYearly"
          ).children[1].children[0].innerText = "";
          document.querySelector(
            ".addons_monthlyOrYearly"
          ).children[1].children[1].innerText = "";
          let addOn_cost = parseInt(priceOfAddOn.split("/")[0].split("$")[1]);
          total -= addOn_cost;
        } else {
          document.querySelector(
            ".addons_monthlyOrYearly"
          ).children[1].children[0].innerText = add_on;

          document.querySelector(
            ".addons_monthlyOrYearly"
          ).children[1].children[1].innerText = priceOfAddOn;
          let addOn_cost = parseInt(priceOfAddOn.split("/")[0].split("$")[1]);
          total += addOn_cost;
        }
        break;
      case "Customizable profile":
        if (
          document.querySelector(".addons_monthlyOrYearly").children[2]
            .children[0].innerText === "Customizable profile"
        ) {
          document.querySelector(
            ".addons_monthlyOrYearly"
          ).children[2].children[0].innerText = "";
          document.querySelector(
            ".addons_monthlyOrYearly"
          ).children[2].children[1].innerText = "";
          let addOn_cost = parseInt(priceOfAddOn.split("/")[0].split("$")[1]);
          total -= addOn_cost;
        } else {
          document.querySelector(
            ".addons_monthlyOrYearly"
          ).children[2].children[0].innerText = add_on;
          document.querySelector(
            ".addons_monthlyOrYearly"
          ).children[2].children[1].innerText = priceOfAddOn;
          let addOn_cost = parseInt(priceOfAddOn.split("/")[0].split("$")[1]);
          total += addOn_cost;
        }
        break;
    }
  }
}
